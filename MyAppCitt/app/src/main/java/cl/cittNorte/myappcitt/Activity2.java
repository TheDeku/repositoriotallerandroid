package cl.cittNorte.myappcitt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),"Presionando pa atras",Toast.LENGTH_SHORT).show();
    }
}
